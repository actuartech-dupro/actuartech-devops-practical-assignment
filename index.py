"""
Main/Entry page
"""
# Import ===============================================================================================================
# The libraries --------------------------------------------------------------------------------------------------------
import numpy as np
import pandas as pd
from dash import dcc
from dash import html
import dash_bootstrap_components as dbc
import os
import logging

from dash.dependencies import Input, Output

# Your other files/pages -----------------------------------------------------------------------------------------------
"""
Remember you have called the file 'app', thus the 'from app'.
The dash object created was named 'app', thus 'import app'.
The server object created was named 'server', thus 'import server'.
The folder containing the pages file is called 'apps', thus the 'from apps'.
"""
# The main app.py file
from initial import dashboard
from initial import server

# Import the file pages
from apps import about_page, contact_page, outline_page, references_page, table_page, overview_page, graphs_page

# Layout settings ======================================================================================================
# Sidebar --------------------------------------------------------------------------------------------------------------
"""
This creates a simple sidebar layout using inline style arguments and the dbc.Nav component.
dcc.Location is used to track the current location, and a callback uses the current location to render the appropriate 
page content. The active prop of each NavLink is set automatically according to the current pathname.
For more details on building multi-page Dash applications, check out the Dash
documentation: https://dash.plot.ly/urls
"""
# the style arguments for the sidebar. We use position:fixed and a fixed width
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "16rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
}

# the styles for the main content position it to the right of the sidebar and
# add some padding.
CONTENT_STYLE = {
    "margin-left": "18rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}

sidebar = html.Div(
    [
        html.H1("COVID-19 Dashboard", className="display-4", style={'font-size': '45px'}),
        html.Hr(),
        dbc.Nav(
            [
                dbc.NavItem(dbc.NavLink("About", href="/apps/about_page", active="exact")),
                dbc.NavItem(dbc.NavLink("Outline of the Dashboard", href="/apps/outline_page", active="exact")),
                dbc.NavItem(dbc.NavLink("Contact", href="/apps/contact_page", active="exact")),
                dbc.NavItem(dbc.NavLink("Further References", href="/apps/references_page", active="exact")),
                dbc.NavItem(dbc.NavLink("Overview", href="/apps/overview_page", active="exact")),
                dbc.NavItem(dbc.NavLink("Graphs", href="/apps/graphs_page", active="exact")),
            ],
            vertical=True,
            pills=True,
        ),
    ],
    style=SIDEBAR_STYLE,
)
# Content and dashboard layout =========================================================================================
# Content layout
content = html.Div(id="page-content", style=CONTENT_STYLE)
# Main app layout
dashboard.layout = html.Div([dcc.Location(id="url"), sidebar, content])


# Dashboard callback
@dashboard.callback(Output(component_id='page-content', component_property='children'),
                    [Input(component_id='url', component_property='pathname')])
def display_page(pathname):
    # if about page was chosen or upon start-up when no page has been chosen yet
    if pathname == '/apps/about_page' or pathname == "/":
        return about_page.layout
    elif pathname == '/apps/contact_page':
        return contact_page.layout
    elif pathname == '/apps/graphs_page':
        return graphs_page.layout
    elif pathname == '/apps/outline_page':
        return outline_page.layout
    elif pathname == '/apps/overview_page':
        return overview_page.layout
    elif pathname == '/apps/references_page':
        return references_page.layout
    elif pathname == '/apps/table_page':
        return description_page.layout
    else:
        return dbc.Container(
            [
                html.H1("404: Not found", className="text-danger"),
                html.Hr(),
                html.P("The pathname was not recognised..."),
            ]
        )


if __name__ == '__main__':
    dashboard.run_server(host="1.2.3.4", port="8000", debug=True)