# actuartech-devops-assignment

This repository serves as the base repository for Actuartech's [Fundamentals of DevOps for Actuarial Data Science](https://www.actuartech.com/courses/fundamentals-of-devops-for-actuarial-data-science) Practical Assignment.

## Guidelines
1. Ensure you are enrolled for the Workshop series. Please contact info@actuartech.com for enquiries.
2. Create a fork of this repository.
3. Clone the repository to your local machine (replace 'user1' with your username).

```git
git clone https://gitlab.com/user1/actuartech-devops-practical-assignment.git
```

4. Follow the instructions detailed in Part I's traceability matrix available on the Actuartech Training Platform through your user profile.
5. Track your progress using Part I's traceability matrix provided on the Actuartech Training Platform course page and commit your changes often.
6. Proceed to Part II using the same forked repository. A separate traceability matrix with Part II's steps will be made available.
7. Once you have completed Part I and Part II, submit a pull request or, alternatively, notify us via email on info@actuartech.com with the link to your repository.
8. Submit a zipped copy of your repository as well as both traceability matrices to the Actuartech Training Platform's submission link.

## Note 
This project has **intentional** errors that should be addressed when working through the assignment. Remember to commit often and commit meaningfully. Refer [here](https://reflectoring.io/meaningful-commit-messages/) for best practices when writing commit messages.

## Contact
Please contact us at info@actuartech.com should you encounter technical difficulties or require further assistance. Please contact us for more information on this workshop series.

## Licence
Property of Actuartech and presented as part of Actuartech's [Fundamentals of DevOps for Actuarial Data Science](https://www.actuartech.com/courses/fundamentals-of-devops-for-actuarial-data-science). All rights reserved.
