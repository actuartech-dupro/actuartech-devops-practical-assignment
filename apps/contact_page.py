from dash import html

layout = html.Div([
    html.H1('Contact'),
    html.P('If you have any questions or would like to learn more, please get in contact with us at: ',
           style={'display': 'inline-block'}
           ),
    html.A("info@actuartech.com", href='info@actuartech.com', target="_blank",
            style={'display': 'inline-block',"margin-left":"5px"}
            ),
    html.P(''),
    html.P('For actuarial data science training needs, please visit: ',
            style={'display': 'inline-block'}
            ),
    html.A("https://training.actuartech.com/",
            href='https://training.actuartech.com/',
            target="_blank",
            style={'display': 'inline-block', "margin-left":"5px"}
            )
    ]
)
