from dash import html

layout = html.Div([
    html.H1('Outline of the Dashboard'),
    html.P('The Dashboard consists of three main pages: “Overview”, “Table”, “Graphs”:'),
    html.H2('Overview'),
    html.P('This page presents information on daily & cumulative case, death, and vaccinations reported. Users can select the region and date they wish to view. In some instances a recent date might not yet be available due to Our World in Data experiencing delays in their records updating process.'),
    html.P('Also note that you may sometimes find negative values. This is a consequence of regions submitting corrected data at a later date to keep the cumulative amounts consistent.'),
    html.H2('Table'),
    html.P('We display a table that can be searched and sorted for users that want a clearer look at the underlying data. Users can select a region to view and a date range.'),
    html.P('Since the number of columns extends past the width of the page, there is a scroll bar at the bottom to slide across.'),
    html.H2('Graphs'),
    html.P('This page provides a platform to create line graphs. Users can select two regions, the feature they want to plot, the date range, and the moving average they would like to apply.'),
    html.P('Between the standard graph and the moving average graph is an output box. This records your mouse activity over the standard graph. Hovering, clicking, double-clicking, and dragging all provide you feedback on the date and corresponding feature you are observing.'),
    html.P('Note that setting the moving average to zero or a negative number will lead to an error. This can be corrected by entering a valid positive moving average.')
    ]
)
